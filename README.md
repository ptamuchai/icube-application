# Icube Application
This application has been created using Spring Boot, Thymeleaf, Zurb Foundation, Java 8 and Maven for dependency management.

***To run the application, follow these steps:***

* Open a command prompt, clone this project to your local machine
* cd to ```icube-application``` (root directory) and execute ```mvn clean install``` to pull dependencies and build the application.
* Use ```mvn spring-boot:run``` to run the application or cd to the target folder and execute ```java -jar icube-app-0.0.1-SNAPSHOT.jar```
* Check the console for test output of the corefunctions (isPrime, isPrimeMemoized and search)
* Open a browser on address ```http://localhost:8085/``` to view the star wars characters
* To mark a character as favorite click on the checkbox on any row
* Click on ```view details``` to navigate to a new tab which shows the character's details

