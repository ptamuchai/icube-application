/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icube.app.functions;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;

/**
 *
 * @author Muchai
 */
public class CoreFunctions {

    //using wrapper classes with Generics
    private static final Function<Integer, Boolean> CACHE = memoize(CoreFunctions::isPrime);

    //function that checks for a positive integer that's divisible by 1 and itself
    public static boolean isPrime(int x) {
        if (x <= 1) {
            return false;
        }
        for (int i = 2; i < x; i++) {
            if (x % i == 0) {
                return false;
            }
        }
        return true;
    }

    //momoize function takes any function as input and returns a memoized version of that function
    public static <T, R> Function<T, R> memoize(Function<T, R> inputFn) {
        ConcurrentMap<T, R> cache = new ConcurrentHashMap<>();
        return (t) -> cache.computeIfAbsent(t, inputFn);
    }

    //memoization of the isPrime function 
    public static Boolean isPrimeMemoized(int x) {
        return CACHE.apply(x);
    }

    //sort array in ascending order before searching
    public static void sortArrayAsc(int[] arr) {
        //swap values in the array if the preceeding value is greater than the succeeding value
        //if no swap occurs, the array is sorted, hence exit
        int length = arr.length;
        int i, j;
        boolean swapped;
        for (i = 0; i < length - 1; i++) {
            swapped = false;
            for (j = 0; j < length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int value = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = value;
                    swapped = true;
                }
            }
            if (swapped == false) {
                break;
            }
        }
    }

    //sort array using binary search algorithm
    public static int search(int[] arr, int val) {
        sortArrayAsc(arr);

        //check if value is equal to the root node value, root node value is at the middle of the sorted array
        //if value is greater than root node value search the right side of the array
        //if value is less than root node value search the left side of the array
        //if value is absent in the array return -1
        int low = 0;
        int high = arr.length - 1;
        while (high >= low) {
            int middle = (low + high) / 2;
            if (arr[middle] == val) {
                return middle;
            }
            if (arr[middle] < val) {
                low = middle + 1;
            }
            if (arr[middle] > val) {
                high = middle - 1;
            }
        }
        return -1;
    }
}
