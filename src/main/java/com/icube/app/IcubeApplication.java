package com.icube.app;

import com.icube.app.functions.CoreFunctions;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IcubeApplication {

    public static void main(String[] args) {
        SpringApplication.run(IcubeApplication.class, args);

        //test integers for functions isPrime() and isPrimeMemoized()
        int x = 1;
        int y = 56;
        int z = 89;
        System.out.println("\n ============ Begin testing Prime Numbers ===========");
        System.out.println(String.format("Is %d a prime number : %b", x, CoreFunctions.isPrime(x)));
        System.out.println(String.format("Is %d a prime number : %b", y, CoreFunctions.isPrime(y)));
        System.out.println(String.format("Is %d a prime number : %b", z, CoreFunctions.isPrime(z)));
        System.out.println(" ============ End testing Prime Numbers ===========\n");

        System.out.println(" ============ Begin testing memoization for the isPrime Function ===========");
        System.out.println(String.format("Is %d a prime number : %b", x, CoreFunctions.isPrimeMemoized(x)));
        System.out.println(String.format("Is %d a prime number : %b", y, CoreFunctions.isPrimeMemoized(y)));
        System.out.println(String.format("Is %d a prime number : %b", z, CoreFunctions.isPrimeMemoized(z)));
        System.out.println(" ============ End testing Memoization for the isPrime Function ===========\n");

        //test array
        int[] arr = {33, 68, 89, 90, 3};
        System.out.println(" ============ Begin testing search function that uses the binary search algorithm ===========");
        System.out.println(String.format("%d is at index  : %d", 89, CoreFunctions.search(arr, 89)));
        System.out.println(String.format("%d is at index  : %d", 56, CoreFunctions.search(arr, 56)));
        System.out.println(String.format("%d is at index  : %d", 90, CoreFunctions.search(arr, 90)));
        System.out.println(" ============ End testing search function that uses the binary search algorithm ===========\n");
    }

}
